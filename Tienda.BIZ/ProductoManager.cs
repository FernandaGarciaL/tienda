﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ProductoManager : GenericManager<producto>, IProductoManager
    {
        public ProductoManager(IGenericRepository<producto> repositorio) : base(repositorio)
        {
        }

        public producto BuscarProductoPorNombreExacto(string nombre)
        {
            return repository.Query(p => p.Nombre == nombre).SingleOrDefault();
        }

        public IEnumerable<producto> BuscarProductosPorNombre(string criterio)
        {
            //Leche fresca
            //Leche Vainilla
            //Leche Natural

            //Leche->3
            //leche->0
            return repository.Query(p => p.Nombre.ToLower().Contains (criterio.ToLower()));
        }
    }
}
