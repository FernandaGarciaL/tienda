﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.UI.WPF
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        IUsuarioManager manager;
        public Login()
        {
            InitializeComponent();
            manager = Tools.FabricManager.UsuarioManager();
        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            usuario user = manager.Login(txtbUsuario.Text, pswPassword.Password);
            if (user != null)
            {
                MessageBox.Show("Bienvenido "+user.Nombres, "Tiendita", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show(manager.Error, "Tiendita", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();//cierra el formulario
        }
    }
}
