﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.COMMON.Entidades
{
    public abstract class BaseDTO:IDisposable
    {
        private bool _isDisposed; //isDisposed le pregunta a la entidad  Dispose si ya se dejo de usar

        public void Dispose()//entidad
        {
            if (!_isDisposed)//cuando ya se dejo de usar
            {
                this._isDisposed = true; //ya se puede eliminar
                GC.SuppressFinalize(this);//con este comando se indica que se puede llevar los datos y liberar recursos en la maquina
            }
        }
    }
}
