﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;
using Tienda.COMMON.Validadores;

namespace Tienda.BIZ
{
    public class FabricManager
    {
        string origen;
        public FabricManager(string origen)
        {
            this.origen = origen;
        }
        public IUsuarioManager UsuarioManager()
        {
            switch (origen)
            {
                case "MySQL":
                    return new UsuarioManager(new DAL.MySQL.GenericRepository<usuario>(new UsuarioValidator(), false));
                case "MSSQL":
                    return new UsuarioManager(new DAL.MSSqlServer.GenericRepository<usuario>(new UsuarioValidator(), false));
                default:
                    return null;
            }
        }
        public IProductoManager ProductoManager()
        {
            switch (origen)
            {
                case "MySQL":
                    return new ProductoManager(new DAL.MySQL.GenericRepository<producto>(new ProductoValidator(), false));
                case "MSSQL":
                    return new ProductoManager(new DAL.MSSqlServer.GenericRepository<producto>(new ProductoValidator(), false));
                default:
                    return null;
            }
        }
        public IVentaManager VentaManager()
        {
            switch (origen)
            {
                case "MySQL":
                    return new VentaManager(new DAL.MySQL.GenericRepository<venta>(new VentaValidator(), false));
                case "MSSQL":
                    return new VentaManager(new DAL.MSSqlServer.GenericRepository<venta>(new VentaValidator(), false));
                default:
                    return null;
            }
        }
        public IProductoVendidoManager ProductoVendidoManager()
        {
            switch (origen)
            {
                case "MySQL":
                    return new ProductoVendidoManager(new DAL.MySQL.GenericRepository<productovendido>(new ProductoVendidoValidator(), false));
                case "MSSQL":
                    return new ProductoVendidoManager(new DAL.MSSqlServer.GenericRepository<productovendido>(new ProductoVendidoValidator(), false));
                default:
                    return null;
            }
        }

    }
}
