﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Tienda.COMMON.Interfaces;


namespace Tienda.DAL.MSSqlServer
{
    public class DBMSSqlServer:IDB
    {
        SqlConnection connection;
        public string Error { get; private set; }
        public DBMSSqlServer()
        {
           //toma la cadena de conexion completa
            string server = @"LAPTOP-VQAGOSPI\SQLEXPRESS";

            string db = "tienda";
            string usr = "tiendauser";
            string pass = "mafer020408c";
            connection = new SqlConnection($"Data Source={server}; Initial Catalog={db};Persist Security Info=True; User Id={usr}; Password{pass}");

            Conectar();
        }
        private bool Conectar()
        {
            try
            {
                connection.Open();
                Error = "";
                return true;
            }
            catch(SqlException ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public bool Comando(string command)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(command, connection);
                cmd.ExecuteNonQuery();
                Error = "";
                return true;
            }
            catch(Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public object Consulta(string consulta)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(consulta, connection);
                SqlDataReader dr = cmd.ExecuteReader();
                Error = "";
                return dr;
            }catch(Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}
