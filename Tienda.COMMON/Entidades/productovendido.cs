﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.COMMON.Entidades
{
    public class productovendido:BaseDTO
    {
        public int IdProductoVendido { get; set; }
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Costo { get; set; }
        public int IdVenta { get; set; }
    }
}
