﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    //tienen que ver con el negocio
    //clases se heredan
    //interfaces se implementan

    /// <summary>
    /// Proporciona métodos estandarizados para el acceso a Tablas; Cada manager creado debe implementar de esta Interfase
    /// </summary>
    /// <typeparam name="T">Tipo de entidad de la cual se implementa el Manager</typeparam>
    //IGenericManager sirve para no escribir el codigo en todos.
    public interface IGenericManager<T> where T:BaseDTO
    {
        /// <summary>
        /// Proporciona el Error relacionado después de alguna operacion
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a Insertar</param>
        /// <returns>Confirmación de la Inserción</returns>
        bool Insertar(T entidad);
        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> ObtenerTodos { get; }
        /// <summary>
        /// Actualizar un registro en la tabla en base a su propiedad Id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla</param>
        /// <returns></returns>
        bool Actualizar(T entidad);
        /// <summary>
        /// Elimina una entidad en base al Id proporcionado
        /// </summary>
        /// <param name="id">Id de la entidad a eliminar </param>
        /// <returns>Confirmación de eliminación</returns>
        bool Eliminar(string id);
        /// <summary>
        /// Obtiene un elemento de acuerdo a su Id 
        /// </summary>
        /// <param name="id">Id del elemento a obtener</param>
        /// <returns>Entidad completa correspondiente al Id proporcionado </returns>
        T BuscarPorId(string id);

    }
}
