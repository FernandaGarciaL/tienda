﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    //al implementar IGenericManager es decir que se trajera el codigo de las operaciones basicas de Manager

    /// <summary>
    /// Proporciona los métodos relacionados a los usuarios
    /// </summary>
    public interface IUsuarioManager:IGenericManager<usuario>
    {

        /// <summary>
        /// Verifica si las credenciales son validas para el usuario 
        /// </summary>
        /// <param name="nombreUsuario">Nombre de usuario</param>
        /// <param name="password">Contraseña del usuario</param>
        /// <returns>Si las credenciales son correctas regresa el usuario completo, de otro modo regresa NULL</returns>
        usuario Login(string nombreUsuario, string password);




    }
}
