﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Validadores
{
    public class UsuarioValidator:AbstractValidator<usuario>
    {
        //ctor doble tabulador para crear constructor
        public UsuarioValidator()
        {
            RuleFor(u => u.Apellidos).NotNull().NotEmpty().Length(1, 50);
            RuleFor(u => u.NombreDeUsuario).NotNull().NotEmpty().Length(50);
            RuleFor(u => u.Nombres).NotNull().NotEmpty().Length(1, 50);
            RuleFor(u => u.Password).NotEmpty().NotNull().Length(1, 50);
        }
    }
}
