﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{

    /// <summary>
    /// Proporciona los métodos relacionados a los productos
    /// </summary>
    public interface IProductoManager:IGenericManager<producto>
    {
        IEnumerable<producto> BuscarProductosPorNombre(string criterio);//regresa un conjunto de productos
        producto BuscarProductoPorNombreExacto(string nombre);

    }
}
