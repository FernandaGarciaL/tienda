﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ProductoVendidoManager : GenericManager<productovendido>, IProductoVendidoManager
    {
        public ProductoVendidoManager(IGenericRepository<productovendido> repositorio) : base(repositorio)
        {
        }

        public IEnumerable<productovendido> ProductosDeUnaVenta(int idVenta)
        {
            return repository.Query(p => p.IdVenta == idVenta);
        }
    }
}
