﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    //el repositorio permite incrementar todas las 4 operaciones basicas de una BD
    //como es genrica permite aceptar cualquier tipo de elementos
    //una Interface también llamadas Contratos define que se va hacer mas no el como.

    /// <summary>
    /// Proporciona los métodos básicos (CRUD) de acceso a una tabla de base de datos
    /// </summary>
    /// <typeparam name="T">Tipo de entidad (Clase) a la que se refiere una tabla </typeparam>
    public interface IGenericRepository<T> where T:BaseDTO //la T es el tipo de elemento que va aceptar. Pero debe cumplir una condición, cuando T herede de BaseDTO quiere decir que es una entidad.
    {
        /// <summary>
        /// Proporciona Información sobre el error ocurrido en alguna de las operaciones
        /// </summary>
        string Error { get; } //solo tendra un método de acceso para proporcionar un error que esta surgiendo en la Bd.

        /* comentario
         * CRUD -> Create (insert), Read (select), Update (update), Delete (delete)
         */
        /// <summary>
        /// Inserta una entidad en la tabla
        /// </summary>
        /// <param name="entidad">Entidad a Insertar</param>
        /// <returns>Confirmación de la Inserción</returns>
        bool Create(T entidad);

        /// <summary>
        /// Obtiene todos los registros de la tabla
        /// </summary>
        IEnumerable<T> Read { get; }

        /// <summary>
        /// Actualiza un registro en la tabla en base a la propiedad id
        /// </summary>
        /// <param name="entidad">Entidad ya modificada, el Id debe existir en la tabla para modificarse</param>
        /// <returns>Confirmación de la Actualización</returns>
        bool Update(T entidad);

        /// <summary>
        /// Elimina en la base de datos de acuerdo al Id relacionado
        /// </summary>
        /// <param name="id">Id de la entidad a eliminar</param>
        /// <returns>Confirmación de eliminación</returns>
        bool Delete(string id);//pasar un string de tipo id. Construir dinamicamente.

        //Query->realizar consultas de acuerdo a la tabla, mediante expresiones lambda
        //IEnumerable regresa un conjunto de elementos 
        /// <summary>
        /// Realiza una consulta personalizada a la tabla
        /// </summary>
        /// <param name="predicado">Expresión Lambda que define la consulta</param>
        /// <returns>Conjunto de entiddaes que cumplen con la consulta</returns>
        IEnumerable<T> Query(Expression <Func<T, bool>> predicado);
        /// <summary>
        /// Obtener una entidad en base a su Id
        /// </summary>
        /// <param name="id">id de la entidad a obtener</param>
        /// <returns>Entidad completa que le corresponde el id proporcionado</returns>
        T SearchById(string id);
    }
}
