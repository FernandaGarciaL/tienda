﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda.COMMON.Entidades
{
    public class venta:BaseDTO
    {
        public int IdVenta { get; set; }
        public DateTime FechaHora { get; set; }
        public string NombreUsuario { get; set; }
        public string Cliente { get; set; }

    }
}
